package ollehto;

import java.util.ArrayList;


public abstract class Etat {
	protected Joueur joueur;
	protected ArrayList<Etat> successeurs;
	
	public Etat(){
		this.successeurs = new ArrayList<>();
	}
	
	public abstract void genereEtat();
	
	public abstract boolean estFinal();
	
	public abstract boolean equals(Object obj);
	
	public abstract String toString();
	
}

