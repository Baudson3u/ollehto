package ollehto;

import java.util.ArrayList;
import java.util.Observable;


public class Model extends Observable implements Runnable{
	
	private boolean finPartie;
	private int infini;
		
	private EtatOllehto etatCourrant;
	private Plateau plateauAffichage;	
	
	private int noir; //0 = humain, >0 = ia
	private int blanc;
	private int[][] tabPoidNoir; // Deux ia peuvent avoir des difficultés différentes
	private int[][] tabPoidBlanc;
	
	private long temps;
		
	public Model(int taillePlateau, int noir, int blanc){
		this.initPartie(taillePlateau, noir, blanc);
		this.initEtat();
	}

	public void update(){
		setChanged();
		notifyObservers();
		clearChanged();	
	}																																		/*** FONCTION D'INITIALISATION ***/
	
	
	/**
	 * Initialise une partie
	 * @param taillePlateau
	 * @param noir
	 * @param blanc
	 */
	public void initPartie(int taillePlateau, int noir, int blanc){
		//Met la fin de partie a faux
		this.finPartie = false;
		
		//Stock les joueurs
		this.noir = noir;
		this.blanc = blanc;
		//Initialise les joueurs 
		JoueurOllehto jNoir, jBlanc;
		jNoir = this.initJoueurNoir();
		jBlanc = this.initJoueurBlanc();	

		//Met les joueur adverse de chacun des joueurs
		this.initJoueur(jNoir, jBlanc);
		
		//par default le joueur 1 commence quelque soit sa couleur
		this.etatCourrant = new EtatOllehto(new Plateau(taillePlateau), jNoir); 
		
		//Valeur de infini pour minimax
		this.infini = taillePlateau*taillePlateau*100;
		
		//Initialise les tableau de poids si un joueur est une ia
		this.initiTabPoid(taillePlateau, noir, blanc);	
	}
	
	public void initEtatIA(){
		//bloquer tout le plateau
//		this.setBloquerPlateau(false);
		
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		//mets nouvelle etat courrant
		if(this.getJoueurCourantCouleur() == Plateau.getNoir()){
			this.setEtatSuivant(this.minimax(this.getNbCoupAvance(this.noir), this.etatCourrant, this.tabPoidNoir));
		}
		else{
			this.setEtatSuivant(this.minimax(this.getNbCoupAvance(this.blanc), this.etatCourrant, this.tabPoidBlanc));
		}
		
	}
	
	/**
	 * Mets le plateau du nouvel etat courant, genere ses successeurs, et gère si c'est un humain ou une machine
	 */
	public void initEtat(){
		this.updateScreen();
		
		//Si ce n'est pas un etat final
		if(!this.estEtatFinal()){	
			//Si le joueur courrant est une machine
			if(this.joueurCourantEstIA()){	
//				System.out.println("JOUEUR IA");
				this.initEtatIA();
			}
			//Si le joueur courrant est un humain
			else{
				// Ne rien faire de spéciale
			}
		}
		//Si c'est un etat final
		else{
			this.finPartie = true;
			this.update();
		}
	}
	
	public void updateScreen(){
		//On Change le plateau
		this.setPlateauPlateauEtat();
//		System.out.println(this.plateauAffichage);
		
		//On genere les successeurs de l'etat courant
//		System.out.println("GENER LES ETATS");
		this.etatCourrant.genereEtat();		
		this.update();
	}
	
	/**
	 * Initialise les adversaires des joueurs
	 * @param noir
	 * @param blanc
	 */
	public void initJoueur(JoueurOllehto noir, JoueurOllehto blanc){
		noir.setAdversaire(blanc);
		blanc.setAdversaire(noir);
	}
	
	/**
	 * Initialise le joueur blanc
	 * @param blanc
	 * @return
	 */
	public JoueurOllehto initJoueurBlanc(){
		//Si le joueur blanc est une machine
		if(this.blanc > 0){
			return new JoueurOllehto("MachineBlanc", Plateau.getBlanc(), true);
		}
		else
			return new JoueurOllehto("JoueurBlanc", Plateau.getBlanc(), false);
	}
	
	/**
	 * Initialise le joueur noir
	 * @param noir
	 * @return
	 */
	public JoueurOllehto initJoueurNoir(){
		//Si le joueur noir est une machine
		if(this.noir > 0){
			return new JoueurOllehto("MachineNoir", Plateau.getNoir(), true);
		}
		else
			return new JoueurOllehto("JoueurNoir", Plateau.getNoir(), false);
	}
	
	/**
	 * Initialise les tableaux de poids pour les joueurs qui sont une ia, 0 = humain, >0 = ia
	 * @param taillePlateau
	 * @param noir
	 * @param blanc
	 */
	public void initiTabPoid(int taillePlateau, int noir, int blanc){
		//Si noir > 0 on veut une ia pour le joueur noir
		if(noir > 0){
//			System.out.println("NOIR = "+noir);
			this.tabPoidNoir = this.getTabPoidDifficulte(noir, taillePlateau);
		}
		else{
			this.tabPoidNoir = null;
		}
		//Si blanc > 0 on veut une ia pour le joueur blanc
		if(blanc > 0){
//			System.out.println("BLANC = "+blanc);
			this.tabPoidBlanc = this.getTabPoidDifficulte(blanc, taillePlateau);
		}
		else{
			this.tabPoidBlanc = null;
		}
	}
																															/*** FONCTION POUR IA ***/
	/**
	 * Retourne le max entre a et b
	 * @param a
	 * @param b
	 * @return
	 */
	public int max(int a, int b){
		return (a > b)? a : b;
	}
	
	/**
	 * Retourne le min entre a et b
	 * @param a
	 * @param b
	 * @return
	 */
	public int min(int a, int b){
		return (a > b)? b : a;
	}
	
	/**
	 * Renvoie la valeur d'un etat pour une ia, selon le tableau tabPoid qui gère la difficulté
	 * @param e
	 * @return
	 */
	public int eval0(EtatOllehto e, int[][] tabPoid){		
		int[] tab = new int[2];
		tab[0] = 0; // Joueur blanc
		tab[1] = 0; // Joueur noir
		int size = e.getPlateau().getTailleLigne();
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				if(e.getCase(i, j) == Plateau.getBlanc()){
					tab[0] += tabPoid[i][j];
				}else if(e.getCase(i, j) == Plateau.getNoir()){
					tab[1] += tabPoid[i][j];
				}
			}
		}
//		return 3;
		
		if(this.getJoueurCourantCouleur()==Plateau.getBlanc()){	
			return tab[0]-tab[1];
		}else{		
			return tab[1]-tab[0];
		}
	}
	
	public int evaluation(int c, EtatOllehto e, int[][] tabPoid){
//		System.out.println("GENERER EVAL");

		e.genereEtat();
		
//		System.out.println("ETAT EVAL:");
//		System.out.println(e);
		//System.out.println();

		if (e.estFinal()){
			this.valeurEtatFinal(e);
		}

		if (c == 0){
			return eval0(e, tabPoid);
		}

		int score_max, score_min;		
		ArrayList<Etat> S = e.getSuccesseurs();

		//Si le joueur de l'etat et le joueur courrant
		if (e.getCouleurJoueur() == this.getJoueurCourantCouleur()){
			score_max = -infini;
			for(Etat s : S){
				score_max = max(score_max, evaluation(c-1, (EtatOllehto)s, tabPoid));
			}
			//Supprime les successeurs une fois utiliser pour ne pas cree de soucis par la suite
//			e.clearSuccesseurs();
			return score_max;
		}
		//Si le joueur de l'etat et le joueur adverse du joueur courant
		else{
			score_min = infini;
			for(Etat s : S){
				score_min = min(score_min, evaluation(c-1, (EtatOllehto)s, tabPoid));
			}
			//Supprime les successeurs une fois utiliser pour ne pas creer de soucis par la suite
//			e.clearSuccesseurs();
			return score_min;
		}
	}
	
	public int evaluation_alpha_beta(int c, EtatOllehto e, int[][] tabPoid, int alpha, int beta){
		e.genereEtat();
		
		if (e.estFinal()){
			this.valeurEtatFinal(e);
		}

		if (c == 0){
			return eval0(e, tabPoid);
		}

		int score_max, score_min;
		int alphaTemp = alpha;
		int betaTemp = beta;
		ArrayList<Etat> S = e.getSuccesseurs();

		//Si le joueur de l'etat est le joueur courrant, on cherche un max
		if (e.getCouleurJoueur() == this.getJoueurCourantCouleur()){
			score_max = -infini;
			for(Etat s : S){
				score_max = max(score_max, evaluation_alpha_beta(c-1, (EtatOllehto)s, tabPoid, alphaTemp, betaTemp));
				if(score_max >= beta){
					return score_max;
				}
				alphaTemp = this.max(alphaTemp, score_max);
			}
			return score_max;
		}
		//Si le joueur de l'etat et le joueur adverse du joueur courant
		else{
			score_min = infini;
			for(Etat s : S){
				score_min = min(score_min, evaluation_alpha_beta(c-1, (EtatOllehto)s, tabPoid, alphaTemp, betaTemp));
				if(score_min <= alpha){
					return score_min;
				}
				betaTemp = this.min(beta, score_min);
			}
			return score_min;
		}
	}
	
	public int valeurEtatFinal(EtatOllehto e){
		int[] tab = e.getComptageCase();
		if (e.getCouleurJoueur() == Plateau.getBlanc()){
			if (tab[0] > tab[1]){
				return this.infini;
			}else{ 
				if(tab[0] < tab[1]){
					return -this.infini;
				}
			}
		}
		return 0;
	}
	
	/**
	 * Retourne l'etat choisit par une ia
	 * @param c
	 * @param e
	 * @param tabPoid
	 * @return
	 */
	public EtatOllehto minimax(int c, EtatOllehto e, int[][] tabPoid){
		ArrayList<Etat> S = e.getSuccesseurs();		
		int score_max = -infini;
		int score;
		EtatOllehto e_sortie = null;
//		System.out.println("minimax");
		
		for(Etat s : S){
//			score = evaluation(c-1, (EtatOllehto)s, tabPoid);
			score = evaluation_alpha_beta(c, (EtatOllehto)s, tabPoid, this.infini, this.infini);
			if (score >= score_max){
				e_sortie = (EtatOllehto)s;
				score_max = score;
			}
			((EtatOllehto)s).clearSuccesseurs();
		}
		return e_sortie;
	}
	
	/**
	 * Change une case du plateau d'affichage
	 * @param ligne
	 * @param colonne
	 * @param couleur
	 */
	public void changerCase(int ligne, int colonne, int couleur){
		this.getPlateau().setCase(ligne, colonne, couleur);
		this.update();
	}
	
	/**
	 * Change le plateau d'affichage
	 * @param ligne
	 * @param colonne
	 */
	public void affichageCoupJouable(int ligne, int colonne){
		int index = this.estCoupJouable(ligne, colonne);
		if(index >= 0)
			for(Case c: this.etatCourrant.getCoupJouable().get(index))
				this.changerCase(c.getLigne(), c.getColonne(), this.getJoueurCourantCouleur());
	}

	
	
	/**
	 * Si la case (ligne, colonne) est une case ou on peut jouer retourne l'index de la liste de coup, -1 sinon
	 * @param ligne
	 * @param colonne
	 * @return
	 */
	public int estCoupJouable(int ligne, int colonne){
		int res = -1;
		ArrayList<ArrayList<Case>> listListCoup = this.etatCourrant.getCoupJouable();
		for(int i=0; i<this.etatCourrant.getCoupJouable().size();i++){
			ArrayList<Case> listCoup = listListCoup.get(i);
			//Si la case est une case ou le joueur peut jouer
			if(listCoup.get(listCoup.size()-1).getLigne() == ligne && listCoup.get(listCoup.size()-1).getColonne() == colonne){
				//retourne l'index de la liste de coup
				res = i;
			}
		}
		return res;
	}
		
	
																																/*** GETTERS ***/
	
	public Joueur getJoueurCourant(){
		return this.etatCourrant.getJoueur();
	}
	
	public boolean joueurCourantEstIA(){
		return this.etatCourrant.getTypeJoueur();
	}
	
	public int getJoueurCourantCouleur(){
		return ((JoueurOllehto)this.etatCourrant.getJoueur()).getCouleur();
	}
	
	public boolean joueurCourantEstHumain(){
		boolean res = false;
		if(this.getJoueurCourantCouleur() == Plateau.getNoir()){
			if(this.getValNoir() == 0){
				res = true;
			}
		}
		else{
			if(this.getValBlanc() == 0){
				res = true;
			}
		}
		return res;
	}
	
	public int getValNoir(){
		return this.noir;
	}
	
	public int getValBlanc(){
		return this.blanc;
	}
	
	public boolean estFinPartie(){
		return this.finPartie;
	}
	
	public Plateau getPlateau(){
		return this.plateauAffichage;
	}
	
	public Plateau getPlateauEtat(){
		return this.etatCourrant.getPlateau();
	}
	
	public int getCasePlateauEtat(int ligne, int colonne){
		return this.getPlateauEtat().getCase(ligne, colonne);
	}
	
	public ArrayList<ArrayList<Case>> getCoupJouable(){
		return this.etatCourrant.getCoupJouable();
	}
	
	/**
	 * Si l'etat courrant est un etat final, met a vrai la variable finPartie, renvoie valeur finPartie
	 * @return
	 */
	public boolean estEtatFinal(){
		return this.etatCourrant.estFinal();
	}
	
	public int[] getComptageCase(){
		return this.etatCourrant.getComptageCase();
	}
	
	/**
	 * Retourne un tableau de poid en fonction du niveau de l'ia
	 * @param difficulte
	 * @param taillePlateau
	 * @return
	 */
	public int[][] getTabPoidDifficulte(int difficulte, int taillePlateau){
		int [][] tabRes = new int[this.getPlateauEtat().getTailleLigne()][this.getPlateauEtat().getTailleColonne()];
		
		switch(difficulte){
		case 1: //Cas Facile
			tabRes = this.getTabFacile(taillePlateau);
			break;
		case 2: //Cas Moyen
			tabRes = this.getTabMoyen(taillePlateau);
			break;
		case 3: //Cas Difficile
			tabRes = this.getTabDifficle(taillePlateau);
			break;
		}
		return tabRes;
	}
	
	public int getNbCoupAvancerFacile(){
		return 0;
	}
	
	public int getNbCoupAvancerMoyen(){
		return 4;
	}

	public int getNbCoupAvancerDifficile(){
		return 6;
	}
		
	public int[][] getTabFacile(int taillePlateau){
		int[][] tabRes = new int[taillePlateau][taillePlateau];
		int size = tabRes.length;
		int eloignement = 0;
		
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				if((i == 0 && j == 0) || (i == (size-1) && j == 0) || (i == 0 && j == (size-1)) || (i == (size-1) && j == (size-1))){
					tabRes[i][j ] = size * 2;
				}else{
					if(i < size / 2 - 1){
						eloignement += size/2-1-i;
					}else if(i > size/2){
						eloignement += i-(size/2);
					}
					if(j < size/2-1){
						eloignement += size/2-1-j;
					}else if(j > size/2){
						eloignement += j-(size/2);
					}
					tabRes[i][j] += 2 + eloignement;
					eloignement = 0;
				}
				
//				if(i < size / 2 - 1){
//					eloignement += size/2-1-i;
//				}else if(i > size/2){
//					eloignement += i-(size/2);
//				}
//				if(j < size/2-1){
//					eloignement += size/2-1-j;
//				}else if(j > size/2){
//					eloignement += j-(size/2);
//				}
//				tabRes[i][j] += 2 + eloignement;
//				eloignement = 0;
			}
		}
		return tabRes;
	}
	
	public int[][] getTabMoyen(int taillePlateau){
		int[][] tabRes = new int[taillePlateau][taillePlateau];
		int size = tabRes.length;
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				if((i == 0 && j == 0) || (i == (size-1) && j == 0) || (i == 0 && j == (size-1)) || (i == (size-1) && j == (size-1))){
					tabRes[i][j] = 3;
				}else{
					tabRes[i][j] = 1;
				}
			}
		}
		return tabRes;
	}
	
	public int[][] getTabDifficle(int taillePlateau){
		int[][] tabRes = new int[taillePlateau][taillePlateau];
		int size = tabRes.length;
		int eloignement = 0;
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				if((i == 0 && j == 0) || (i == (size-1) && j == 0) || (i == 0 && j == (size-1)) || (i == (size-1) && j == (size-1))){
					tabRes[i][j]  = taillePlateau*2;
				}else{
					if(i < size / 2 - 1){
						eloignement += size/2-1-i;
					}else if(i > size/2){
						eloignement += i-(size/2);
					}
					if(j < size/2-1){
						eloignement += size/2-1-j;
					}else if(j > size/2){
						eloignement += j-(size/2);
					}
					tabRes[i][j] += taillePlateau - eloignement;
					eloignement = 0;
				}
			}
		}
		
//		for (int[] tab : tabRes){
//			for (int i : tab){
//				System.out.print(" "+i+" ");
//			}
//			System.out.println("");
//		}
		
		return tabRes;
	}
	
	public int getNbCoupAvance(int difficulte){
		int res = 0;
		switch(difficulte){
		case 1:
			res = this.getNbCoupAvancerFacile();
			break;
		case 2:
			res = this.getNbCoupAvancerMoyen();
			break;
		case 3:
			res = this.getNbCoupAvancerDifficile();
			break;
		}
		return res;
	}
	
																																	/*** SETTERS ***/
	
	public void setEtatCourrant(EtatOllehto etat){		
		this.etatCourrant = etat;
		
	}
	
	public void setPlateauPlateauEtat(){
		this.setPlateauAffichage(new Plateau(this.etatCourrant.getPlateau()));
		this.update();
	}
		
	public void setPlateauAffichage(Plateau p){
		this.plateauAffichage = p;
	}

	public void setNouvellePartie(int taille){
		this.finPartie = false;
		this.etatCourrant = new EtatOllehto(new Plateau(this.getPlateau().getTailleColonne()), this.etatCourrant.getJoueur());
//		this.etatCourrant = new EtatOllehto(new Plateau(taille), this.j1);
		infini = this.getPlateau().getTailleColonne()*this.getPlateau().getTailleColonne()*10;
		this.initEtat();
	}
	
	/**
	 * Permet de relancer une partie, remets tout a zero
	 * @param taille
	 */
	public void setNouvellePartie(int taille, int noir, int blanc){
//		//Initialise les joueurs 
//		this.noir = noir;
//		this.blanc = blanc;
//		JoueurOllehto jNoir, jBlanc;
//		jNoir = this.initJoueurNoir();
//		jBlanc = this.initJoueurBlanc();
//
//		//Met les joueur adverse de chacun des joueurs
//		this.initJoueur(jNoir, jBlanc);
//		
//		this.finPartie = false;
//		this.etatCourrant = new EtatOllehto(new Plateau(taille), jNoir);
//		
//		//Initialise l'infini
//		infini = this.getPlateauEtat().getTailleColonne()*this.getPlateau().getTailleColonne()*100;
//				
//		//Initialise les tableau de poids si un joueur est une ia
//		this.initiTabPoid(taille, noir, blanc);
		
		this.initPartie(taille, noir, blanc);
		
		//Lance la partie
		this.initEtat();
	}
	
	/**
	 * Change l'etat, avec l'etat successeur voulu (index de l'etat)
	 * @param index
	 */
	public void setEtatSuivant(int index){		
		this.setEtatCourrant((EtatOllehto)this.etatCourrant.getEtatSucesseur(index));	
		this.initEtat();
	}
	
	/**
	 * Change l'etat, avec l'etat successeur voulu
	 * @param etatSuivant
	 */
	public void setEtatSuivant(Etat etatSuivant){
		if(etatSuivant == null){
			System.out.println("ETAT NULL");
		}
		else{
			this.setEtatCourrant((EtatOllehto)etatSuivant);			
			this.initEtat();
		}
	}

	public void run() {
		//Initialise l'etat courant
		this.initEtat();
	}
	
	

	
}
