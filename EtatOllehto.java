package ollehto;

import java.util.ArrayList;



public class EtatOllehto extends Etat{
	
	private Plateau plateau;
	public ArrayList<ArrayList<Case>> coupJouable;
	
	public EtatOllehto(Plateau plateau, Joueur joueur){
		super();
		this.plateau = plateau;
		this.joueur = joueur;
		this.coupJouable = new ArrayList<>();
	}

	/** 
	 * Genere les etat successeurs
	 **/
	public void genereEtat(){
		//Remplis la liste de liste de coup jouable
		this.genererCoupJouable();
		//Fusionne les liste de coups pour en faire des coups multiples
		this.fusionCoupMultiple();	
//		System.out.println("AVANT TEST: "+this.successeurs.size());
//		System.out.println("Joueur de l'etat: "+this.joueur.getNom());

		//Pour chaque liste de coup de la liste
		for(ArrayList<Case> ac : coupJouable){
//			System.out.println("TEST: "+this.coupJouable.size());
			Plateau p = new Plateau(this.getPlateau());
			//Change les cases par les nouvelles cases
			for(Case c: ac){
				p.setCase(c.getLigne(), c.getColonne(), this.getCouleurJoueur());
			}
			this.successeurs.add(new EtatOllehto(p,((JoueurOllehto)this.getJoueur()).getAdversaire()) );
		}
//		System.out.println("NB SUCC: "+this.successeurs.size());
//		System.out.println();
	}
	
	
	//PEUT ETRE PAS TRES PROPRE (fusioner des coup ou les calculer direct ?)
	public void fusionCoupMultiple(){
		int cpt = 0;
//		System.out.println("NB COUP JOUABLE "+cpt+": "+this.coupJouable.size());
		for(int i=0; i<this.coupJouable.size(); i++){
			ArrayList<Case> list1 = this.coupJouable.get(i);
			for(int j=0; j<this.coupJouable.size();j++){
				ArrayList<Case> list2 = this.coupJouable.get(j);
				//Si la list1 != list2 (on ne veut pas additioner une meme liste)
				if(!list1.equals(list2)){
					//Si la case ou on joue (dernière case de la liste) est la même pour les deux listes
					if(list1.get(list1.size()-1).equals(list2.get(list2.size()-1))){
						//On retire la dernière case la list1 car on va ajouter ceux de la list2 pour eviter des doublons
						list1.remove(list1.size()-1);
						for(int y=0; y<list2.size(); y++){
							list1.add(list2.get(y));
						}
						this.coupJouable.remove(j);
						cpt++;
//						System.out.println("NB COUP JOUABLE "+cpt+": "+this.coupJouable.size());
					}
				}
			}
		}
	}
	
	/**
	 * Remplis une liste de liste de coup, concretement stock la liste des cases qui seront changees si on joue la ou on peut
	 */
	public void genererCoupJouable(){
		for(int i=0;i<this.getPlateau().getTailleLigne();i++)
			for(int j=0;j<this.getPlateau().getTailleColonne();j++)
				if(this.getCase(i,j) == this.getCouleurJoueur())
					this.voisin(i, j);		
	}
	
	
	/**
	 * Cherche les voisins d'une autre couleur autour d'une case, si il en existe appel la fonction voisinAux
	 * @param ligne
	 * @param colonne
	 */
	public void voisin(int ligne, int colonne){	
		for(int i=-1;i<2;i++){
			for(int j=-1;j<2;j++){			
				//verifie que la case ciblé soit bien dans le plateau
				if(this.getPlateau().estDansPlateau(ligne+i, colonne+j)){
					if(this.getCase(ligne+i, colonne+j) == this.getCouleurAdversaire()){					
						//liste de case pour stocker la "droite" de case de la meme couleur
						ArrayList<Case> listCase = new ArrayList<>();
						listCase.add(new Case(ligne+i, colonne+j));
						this.voisinAux(ligne+i, colonne+j, i, j,listCase);
					}
				}
			}
		}
	}
	
	
	/**
	 * VERSION RECURSIVE
	 * Cherche tout les voisins d'une meme couleur dans la même direction, cherche egalement si la dernière case est disponible pour le joueur
	 * @param ligne
	 * @param colonne
	 * @param y
	 * @param x
	 * @param listCase
	 */
	public void voisinAux(int ligne, int colonne, int y, int x, ArrayList<Case> listCase){
		ArrayList<Case> listCaseTemp = listCase;
		//Test si la case est dans le plateau
		if(this.getPlateau().estDansPlateau(ligne+y, colonne+x)){
			if(this.getCase(ligne+y, colonne+x) == this.getCouleurAdversaire()){			
				//ajoute la case dans la lsite de "droite" de case de la meme couleur
				listCaseTemp.add(new Case(ligne+y,colonne+x));		
				//rapelle la fonction sur la dernière case pour voir si dans la meme "droite" il y a encore des cases de la meme couleur
				this.voisinAux(ligne+y,colonne+x,y,x,listCaseTemp);
			}
			else{
				if(this.getCase(ligne+y, colonne+x) == Plateau.getVide()){
					//On ajoute la case final du mouvement (la ou on pourra jouer)
					listCaseTemp.add(new Case(ligne+y,colonne+x));
					this.coupJouable.add(listCaseTemp);
				}
			}			
		}	
	}

	
	/*** GETTER ***/
	
	public boolean getTypeJoueur(){
		return ((JoueurOllehto)this.getJoueur()).estIA();
	}
	
	public char getCharCouleur(int couleur){
		return (couleur < 0)?'V':(couleur == 0)?'B':'N';
	}
	
	public Joueur getJoueur(){
		return this.joueur;
	}
	
	public int getCouleurAdversaire(){
		return (((JoueurOllehto)this.joueur).getCouleur() + 1) % 2;
	}
	
	public int getCouleurJoueur(){
		return ((JoueurOllehto)this.joueur).getCouleur();
	}
	
	public int getCase(int ligne, int colonne){
		return plateau.getCase(ligne,colonne); //GERER LES EXCEPTIONS
	}
	
	public Plateau getPlateau(){
		return this.plateau;
	}
	
	public int[] getComptageCase(){
		int[] tab = new int[2];
		tab[0] = 0; // Joueur blanc
		tab[1] = 0; // Joueur noir
		
		for(int i=0; i<this.getPlateau().getTailleLigne(); i++){
			for(int j=0; j<this.getPlateau().getTailleColonne(); j++){
				if(this.getCase(i, j) == Plateau.getBlanc())
					tab[0]++;
				else if(this.getCase(i, j) == Plateau.getNoir())
					tab[1]++;
			}
		}
		return tab;
	}
	
	public ArrayList<ArrayList<Case>> getCoupJouable(){
		return this.coupJouable;
	}
	
	public Etat getEtatSucesseur(int index){
		if(index >= this.getCoupJouable().size() && index < 0)
			System.out.println("ERREUR D'INDEX DANS GETETATSUCESSEUR INDEX="+index);
		return this.successeurs.get(index); //GERER LES EXCEPTIONS
	}
	
	public ArrayList<Etat> getSuccesseurs(){
		return this.successeurs;
	}
		
	public boolean successeurIsEmpty(){
		return this.successeurs.isEmpty();
	}
	
	/**
	 * Retourne vrai si les deux joueurs ne peuvent plus jouer, faux sinon
	 * @return
	 */
	public boolean blocageSuccesseur(){
		boolean res = false;
		//Si le joueur ne peut plus jouer
		if(this.successeurIsEmpty()){
			//Change le joueur actuel (mets son adversaire)
			this.joueur = ((JoueurOllehto)this.getJoueur()).getAdversaire();
//			System.out.println("GENER DE E.ESTFINAL");
			this.genereEtat();
			//Si son adversaire ne peut plus jouer
			if(this.successeurIsEmpty()){
				res = true;
			}
//			this.successeurs.clear();
		}
		return res;
	}
	
	public boolean estFinal(){
		boolean res = false;
		//Si le plateau est pleins
		if(this.getPlateau().estRemplit()){
			res = true;
		}
		else{
			res = this.blocageSuccesseur();
		}
		return res;
	}
	
	public void clearSuccesseurs(){
		this.successeurs.clear();
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		
		EtatOllehto eTemp = (EtatOllehto)obj;
		
		if(!this.getJoueur().equals(eTemp.getJoueur()))
			return false;
		
		if(!this.getPlateau().equals(eTemp.getPlateau()))
			return false;
		
		return true ;
	}

	public String toString() {
		StringBuilder strB = new StringBuilder();
		//affichage du plateau
		strB.append(this.plateau.toString());
		//Affichage du nom joueur
		strB.append("Joueur: "+this.joueur.getNom());
		return strB.toString();
	}	
	
	public static void main(String[] args){
		Plateau p = new Plateau(8);
		Plateau p2 = new Plateau(8);

		JoueurOllehto j = new JoueurOllehto("Clement",Plateau.getNoir(),false);
		JoueurOllehto j2 = new JoueurOllehto("Lucas",Plateau.getBlanc(),false);
		j2.setAdversaire(j);
		j.setAdversaire(j2);
		
//		p2.setCase(4, 3,Plateau.getBlanc());
//		p2.setCase(5, 4,Plateau.getNoir());
		
		p2.setCase(2,3,Plateau.getNoir());
		p2.setCase(6,3,Plateau.getNoir());
		p2.setCase(5, 2, Plateau.getBlanc());
		p2.setCase(7, 0, Plateau.getNoir());
		p2.setCase(6, 1, Plateau.getBlanc());
		p2.setCase(4, 1, Plateau.getBlanc());
		p2.setCase(4, 1, Plateau.getBlanc());
		p2.setCase(4, 2, Plateau.getBlanc());
		p2.setCase(4, 0, Plateau.getNoir());
		p2.setCase(5, 1, Plateau.getNoir()); 

		
		EtatOllehto e0 = new EtatOllehto(p, j);
		EtatOllehto e1 = new EtatOllehto(p, j);
		EtatOllehto e2 = new EtatOllehto(p, j2);
		EtatOllehto e3 = new EtatOllehto(p2, j);
		
		System.out.println(e3);
		
		e3.genereEtat();
		e3.blocageSuccesseur();
		
	}

}
