package ollehto.graphique;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import ollehto.Model;

public class Ollehto extends JFrame{
	
	public Ollehto(){
		super("Ollehto le reversi d'Othello");
				
				
		int taillePlateau = 8;

		//0 = joueur humain, >0 = joueur machine
		int noir, blanc;
		noir = 0;
		blanc = 1;
		
		//Model m
		Model m = new Model(taillePlateau, noir, blanc);
		
		VueGrille vg = new VueGrille(m);
		this.add(vg, BorderLayout.CENTER);
		
		VueDroite vd = new VueDroite(m);
		this.add(vd, BorderLayout.EAST);
		
		VueMenu vm = new VueMenu(m);
		this.setJMenuBar(vm);
								
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(1100, 800));
		setVisible(true) ;
		this.pack() ;
	}
	
	public static void main(String[] a) {
		new  Ollehto();
	}
}

/*
 * LE JOUEUR NOIR EST PLUS LONG A JOUER, TOUJOURS LA MEME FIN
 */
