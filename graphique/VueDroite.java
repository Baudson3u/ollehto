package ollehto.graphique;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import ollehto.Model;

public class VueDroite extends JPanel implements Observer{

	private Model m;
	private JRadioButton noirHumain;
	private JRadioButton noirMachine;
	private JRadioButton facileNoir;
	private JRadioButton moyenNoir;
	private JRadioButton difficileNoir;
	
	private JRadioButton blancHumain;
	private JRadioButton blancMachine;
	private JRadioButton facileBlanc;
	private JRadioButton moyenBlanc;
	private JRadioButton difficileBlanc;
	
	private JButton nouvellePartie;
		
	
	public VueDroite(Model model){
		this.m = model;
		
		this.setLayout(new GridLayout(5,3));
		
		/*** BOUTON JOUEUR NOIR ***/
		
		this.add(new JLabel("NOIR: "));
		
		this.noirHumain = new JRadioButton("Humain");
		this.noirHumain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				noirMachine.setSelected(false);
				facileNoir.setVisible(false);
				moyenNoir.setVisible(false);
				difficileNoir.setVisible(false);
			}
		});
		this.add(this.noirHumain);
		
		
		this.noirMachine = new JRadioButton("Machine");
		this.noirMachine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!noirMachine.isSelected()){
					noirHumain.setSelected(true);
					facileNoir.setVisible(false);
					moyenNoir.setVisible(false);
					difficileNoir.setVisible(false);
				}else{
					noirHumain.setSelected(false);
					facileNoir.setVisible(true);
					moyenNoir.setVisible(true);
					difficileNoir.setVisible(true);
				}
			}
		});
		this.add(this.noirMachine);
		
		//Bouton difficulté facile noir
		this.facileNoir = new JRadioButton("Facile");
		this.facileNoir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				moyenNoir.setSelected(false);
				difficileNoir.setSelected(false);
			}
		});
		this.facileNoir.setVisible(false);
		this.add(facileNoir);
		
		//Bouton difficulté moyen noir
		this.moyenNoir = new JRadioButton("Moyen");
		this.moyenNoir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				facileNoir.setSelected(false);
				difficileNoir.setSelected(false);
			}
		});
		this.moyenNoir.setVisible(false);
		this.add(moyenNoir);
		
		//Bouton difficulté difficile noir
		this.difficileNoir = new JRadioButton("Difficile");
		this.difficileNoir.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				facileNoir.setSelected(false);
				moyenNoir.setSelected(false);
			}
		});
		this.difficileNoir.setVisible(false);
		this.add(difficileNoir);
		
		/*** BOUTON JOUEUR BLANC ***/
		
		this.add(new JLabel("BLANC: "));
		this.blancHumain = new JRadioButton("Humain");
		this.blancHumain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				blancMachine.setSelected(false);
				facileBlanc.setVisible(false);
				moyenBlanc.setVisible(false);
				difficileBlanc.setVisible(false);				
			}
		});
		this.add(this.blancHumain);
		
		this.blancMachine = new JRadioButton("Machine");
		this.blancMachine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!blancMachine.isSelected()){
					blancHumain.setSelected(true);
					facileBlanc.setVisible(false);
					moyenBlanc.setVisible(false);
					difficileBlanc.setVisible(false);
				}else{
					blancHumain.setSelected(false);
					facileBlanc.setVisible(true);
					moyenBlanc.setVisible(true);
					difficileBlanc.setVisible(true);
				}
			}
		});
		this.add(this.blancMachine);
		
		//Bouton difficulté facile blanc
		this.facileBlanc = new JRadioButton("Facile");
		this.facileBlanc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				moyenBlanc.setSelected(false);
				difficileBlanc.setSelected(false);
			}
		});
		this.facileBlanc.setVisible(false);
		this.add(facileBlanc);
		
		//Bouton difficulté moyen blanc
		this.moyenBlanc = new JRadioButton("Moyen");
		this.moyenBlanc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				facileBlanc.setSelected(false);
				difficileBlanc.setSelected(false);
			}
		});
		this.moyenBlanc.setVisible(false);
		this.add(moyenBlanc);
		
		//Bouton difficulté difficile blanc
		this.difficileBlanc = new JRadioButton("Difficile");
		this.difficileBlanc.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				facileBlanc.setSelected(false);
				moyenBlanc.setSelected(false);
			}
		});
		this.difficileBlanc.setVisible(false);
		this.add(difficileBlanc);
		
		/*** BOUTON NOUVELLE PARTIE ***/
		
		this.nouvellePartie = new JButton("Nouvelle partie");
		this.nouvellePartie.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent arg0) {
				lancerNouvellePartie();
			}
		});
		this.add(this.nouvellePartie);
			
		m.addObserver(this);
	}
	
	public int getValNoir(){
		int res = 0;
		if(noirMachine.isSelected()){
			if(facileNoir.isSelected()){
				res = 1;
			}
			else{
				if(moyenNoir.isSelected()){
					res = 2;
				}
				else{
					res = 3;
				}
			}
		}
		return res;
	}
	
	public int getValBlanc(){
		int res = 0;
		if(blancMachine.isSelected()){
			if(facileBlanc.isSelected()){
				res = 1;
			}
			else{
				if(moyenBlanc.isSelected()){
					res = 2;
				}
				else{
					res = 3;
				}
			}
		}
		return res;
	}
	
	public void lancerNouvellePartie(){
		int taillePlateau = m.getPlateauEtat().getTailleColonne();
		int noir,blanc; // 0 = humain, 1-3 = ia et la difficulté selon la valeur
		noir = this.getValNoir();
		blanc = this.getValBlanc();

		m.initPartie(taillePlateau, noir, blanc);
		Thread t = new Thread((Runnable)m,"olletho");
		t.start();
		
//		m.setNouvellePartie(taillePlateau, noir, blanc);
	}

	public void update(Observable arg0, Object arg1) {
		if(noirMachine.isSelected()){
			this.facileNoir.setVisible(true);
		}
	}


}
