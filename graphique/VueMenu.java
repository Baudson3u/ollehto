package ollehto.graphique;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import ollehto.Model;
import ollehto.Plateau;

public class VueMenu extends JMenuBar implements Observer{

	private Model m;
	private JMenu partie;
	private JMenuItem nouvellePartie;
	private JLabel joueurCourant;
		
	public VueMenu(Model model){
		this.m = model;
		
		this.partie = new JMenu("Partie");
		
		this.nouvellePartie = new JMenuItem("Nouvelle partie");
		this.nouvellePartie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				m.setNouvellePartie(8);
			}
		});
		
		this.partie.add(this.nouvellePartie);
		this.add(partie);

		this.joueurCourant = new JLabel();
		this.setLabelJoueurCourant();
		this.add(this.joueurCourant);

		m.addObserver(this);
	}
	
	public void setLabelJoueurCourant(){
		if(this.m.getJoueurCourantCouleur() == Plateau.getBlanc()){
			this.joueurCourant.setText("    Joueur: Blanc");

		}
		else{
			this.joueurCourant.setText("    Joueur: Noir");
		}
	}

	public void update(Observable arg0, Object arg1) {
		this.setLabelJoueurCourant();
				
		if(this.m.estFinPartie()){
			int[] tab = this.m.getComptageCase();
			this.joueurCourant.setText("    Blanc: "+tab[0]+"  Noir: "+tab[1]); //TEMPORAIRE
		}
	}
	

}
