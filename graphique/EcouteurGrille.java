package ollehto.graphique;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import ollehto.JoueurOllehto;
import ollehto.Model;
import ollehto.Plateau;

public class EcouteurGrille extends MouseAdapter{

	private Model m;
	private int ligne;
	private int colonne;
	
	public EcouteurGrille(int ligne, int colonne, Model model){
		this.ligne = ligne;
		this.colonne = colonne;
		m = model;
	}
	
	public void mouseEntered(MouseEvent arg0){
		if(!m.estFinPartie()){ //Stop les calcules quand le jeux est finit
			if(this.m.joueurCourantEstHumain()){
				m.affichageCoupJouable(this.ligne, this.colonne);
			}
		}
	}
	
	public void mouseExited(MouseEvent arg0) {
		//Si on quitte la case on remets le plateau de l'etat courant
		this.m.setPlateauPlateauEtat();
	}
	
	
	public void mouseClicked(MouseEvent e){
		if (SwingUtilities.isLeftMouseButton(e)){
			int indexEtatSuivant = m.estCoupJouable(ligne, colonne); 
			if(indexEtatSuivant >= 0){
				m.setEtatSuivant(indexEtatSuivant);
			}
		}
	}

}
