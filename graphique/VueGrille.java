package ollehto.graphique;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ollehto.Case;
import ollehto.Model;
import ollehto.Plateau;

public class VueGrille extends JPanel implements Observer{
	
	private Model m;
	private JButton[][] tjb;
	
	public VueGrille(Model model){
		m = model;
		int ligne = m.getPlateau().getTailleLigne();
		int colonne = m.getPlateau().getTailleColonne();
		this.setLayout(new GridLayout(ligne, colonne));
		tjb = new JButton[ligne][colonne];
		
		for(int i = 0; i<ligne; i++){
			for(int j = 0; j<colonne; j++){
				tjb[i][j] = new JButton();
				tjb[i][j].addMouseListener((MouseListener) new EcouteurGrille(i, j, m)) ;
				tjb[i][j].setBackground(Color.lightGray);
				this.add(tjb[i][j]);
			}
		}

		m.addObserver(this);
		m.update();
	}
	
	/**
	 * Remplit les cases de la grille
	 */
	public void remplitGrille(){
		Plateau p = m.getPlateau();
						
		for(int i = 0; i<p.getTailleLigne();i++){
			for(int j = 0; j<p.getTailleColonne();j++){
				switch(p.getCase(i, j)){
				   //cas vide
				   case -1 :
					   tjb[i][j].setBackground(Color.lightGray);
//					   tjb[i][j].setEnabled(false);
					   break;
				   //cas blanc
				   case 0:
					   tjb[i][j].setBackground(Color.WHITE);
//					   tjb[i][j].setEnabled(false);
					   break;
				   //cas noir
				   case 1:
					   tjb[i][j].setBackground(Color.BLACK);
//					   tjb[i][j].setEnabled(false);
					   break;
				}
			}
		}
	}
	
	public void setEnablePlateau(boolean bool){
		Plateau p = m.getPlateau();
		for(int i = 0; i<p.getTailleLigne();i++){
			for(int j = 0; j<p.getTailleColonne();j++){
			   tjb[i][j].setEnabled(bool);
			}
		}
	}
	
	public void remplitCoupJouable(){
		//affichage des coups jouables par le joueur courant
		ArrayList<ArrayList<Case>> coupJouables = m.getCoupJouable();
		for(ArrayList<Case> ac : coupJouables){
			int ligne = ac.get(ac.size()-1).getLigne();
			int colonne = ac.get(ac.size()-1).getColonne();
			if(this.m.getPlateau().getCase(ligne, colonne) == Plateau.getVide()){
				tjb[ac.get(ac.size()-1).getLigne()][ac.get(ac.size()-1).getColonne()].setBackground(Color.DARK_GRAY);
				tjb[ac.get(ac.size()-1).getLigne()][ac.get(ac.size()-1).getColonne()].setEnabled(false);
			}
		}
	}
	
	public void popUpFinPartie(){
		JOptionPane d = new JOptionPane(); 
		String lesTextes[]= { "Nouvelle partie", "Quitter le jeu" }; 
		int retour = d.showOptionDialog(this, "le message", "Gagné", 0, JOptionPane.INFORMATION_MESSAGE, /* icone*/ null, /* les textes de boutons*/ lesTextes, /* le bouton par défaut*/ lesTextes[0]); 
		if( retour == 0){
//			String s = (String)JOptionPane.showInputDialog(null,"Une nouvelle partie ?","Gagné !",JOptionPane.INFORMATION_MESSAGE,null, null,null);//choix, choix[0]) ;
//			if (s!=null){
//				int taille = Integer.parseInt(s);
//				if (taille>2 && taille <=20 && taille%2==0){
					m.setNouvellePartie(4);
//				}else{
//					m.setNouvellePartie(8);
//				}
//			}
		}else if( retour == 1){
			System.exit(0);
		}
	}

	public void update(Observable arg0, Object arg1) {
		this.remplitGrille();
		
		if(this.m.joueurCourantEstHumain()){
			this.remplitCoupJouable();
		}
		
		this.repaint();
		
		if(this.m.estFinPartie()){
//			this.popUpFinPartie();
		}
	}


}
