package ollehto;


public abstract class Joueur {
	protected String nom;
	protected boolean ia;

	public Joueur(String nomJoueur, boolean estOrdi){
		this.nom = nomJoueur;
		this.ia = estOrdi;
	}
	
	public boolean estIA(){
		return this.ia;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		
		if(this.getNom() != ((Joueur)obj).getNom())
			return false;
		
		return  true;
	}
	

}
