package ollehto;

public class Case {

	private int ligne;
	private int colonne;
	
	
	public Case(int ligne, int colonne){
		this.ligne = ligne;
		this.colonne = colonne;
	}	
	
	public int getLigne(){
		return this.ligne;
	}
	
	public int getColonne(){
		return this.colonne;
	}
	
	public String toString(){
		return "("+this.ligne+","+this.colonne+")";
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Case other = (Case) obj;
		if (colonne != other.colonne)
			return false;
		if (ligne != other.ligne)
			return false;
		return true;
	}


}
