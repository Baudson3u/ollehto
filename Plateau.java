package ollehto;

/**
 * @author clem
 *
 */
public class Plateau {
	
	private int tab[][];
	private static int vide = -1; //case vide
	private static int blanc = 0; //case boule blanche
	private static int noir = 1; //case boule noire
	
//	private  int vide = -1; //case vide
//	private  int blanc = 0; //case boule blanche
//	private  int noir = 1; //case boule noire
	
	public Plateau(int n){
		this.tab = new int[n][n];
		//Initialise le plateau
		this.initPlateau();
	}
	
	/**
	 * Constructeur par copie
	 * @param p
	 */
	public Plateau(Plateau p){
		this.tab = new int[p.getTailleLigne()][p.getTailleColonne()];
		//copie le plateau
		this.copiePlateau(p);
	}
	
	/**
	 * copie un plateau
	 * @param p
	 */
	public void copiePlateau(Plateau p){
		for(int i=0;i<p.getTailleLigne();i++){
			for(int j=0;j<p.getTailleColonne();j++){
				this.tab[i][j] = p.getCase(i, j);
			}
		}
	}
	
	/**
	 * Initialise un plateau de base
	 */
	public void initPlateau(){
		//Initialise les case a -1 (vide)
		for(int i=0;i<this.getTailleLigne();i++)
			for(int j=0;j<this.getTailleColonne();j++)
				this.tab[i][j] = Plateau.getVide();
		
		//case haut gauche du carré de case centrale 
		int ligne = (this.getTailleColonne() - 2)/2;
		int colonne = (this.getTailleLigne() - 2)/2;
		
		//Initialise les cases près remplit
		tab[ligne][colonne] = Plateau.getBlanc();
		tab[ligne+1][colonne+1] = Plateau.getBlanc();
		tab[ligne+1][colonne] = Plateau.getNoir();
		tab[ligne][colonne+1] = Plateau.getNoir();		
	}
	
	/*** SETTERS ***/
	
	public void setCase(int ligne, int colonne, int val){
		//Change la case si elle est bien comprise dans le plateau
		if(this.estDansPlateau(ligne, colonne))
			this.tab[ligne][colonne] = val;
		else
			System.out.println("ERREUR DANS PLATEAU.SETCAS(), VALEUR HORS PLATEAU\n");
	}
	
	/*** GETTERS ***/
	
	public int getCase(int ligne, int colonne){ //GERER LES EXCEPTIONS
		if(!this.estDansPlateau(ligne, colonne))
			System.out.println("GETCASE(LIGNE, COLONNE) HORS TABLEAU");	
		return this.tab[ligne][colonne];
	}
	
	public int getTailleLigne(){
		return this.tab.length;
	}
	
	public int getTailleColonne(){
		return this.tab[0].length;
	}
	
	public static int getVide(){
		return vide;
	}
	
	public static int getBlanc(){
		return blanc;
	}
	
	public static int getNoir(){
		return noir;
	}
	
	public static char getCharCouleur(int couleur){
		return (couleur < 0)?'V':(couleur == 0)?'B':'N';
	}
	
	public int[][] getGrille(){
		return this.tab;
	}
	
	/**
	 * Retourne vrai si le la case voulu est dans le plateau
	 * @param ligne
	 * @param colonne
	 * @return
	 */
	public boolean estDansPlateau(int ligne, int colonne){
		return ligne >= 0 && ligne < this.getTailleLigne() 
				&& colonne >= 0 && colonne < this.getTailleLigne();
	}
	
	/**
	 * retourne vrai si le plateau est remplit
	 * @return
	 */
	public boolean estRemplit(){
		boolean res = true;
		for(int i=0; i<this.getTailleLigne();i++){
			for(int j=0; j<this.getTailleColonne();j++){
				if(this.getCase(i, j) == Plateau.getVide())
					res = false;
			}
		}
		return res;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		
		if(this.getTailleColonne() != ((Plateau)obj).getTailleColonne())
			return false;
		
		if(this.getTailleLigne() != ((Plateau)obj).getTailleLigne())
			return false;
		
		for(int i=0;i<this.getTailleLigne();i++){
			for(int j=0;j<this.getTailleColonne();j++){
				if(this.getCase(i, j) != ((Plateau)obj).getCase(i, j))
					return false;
			}
		}
		
		return true;
	}

	public String toString() {
		StringBuilder strB = new StringBuilder();
		
		//Affiche le numero des cases
		strB.append("  ");
		for(int i=0;i<this.getTailleLigne();i++){
			strB.append(" "+i+" ");
		}
		strB.append("\n");
		
		for(int i=0;i<this.getTailleLigne();i++){
			strB.append(i+" ");
			for(int j=0;j<this.getTailleColonne();j++){
				//Cas vide
				if(this.getCase(i,j) == -1)
					strB.append("[ ]");
				//Case blanc
				if(this.getCase(i,j) == 0)
					strB.append("[B]");
				//cas noire
				if(this.getCase(i,j) == 1)
					strB.append("[N]");
			}
			strB.append("\n");
		}
		return strB.toString();
	}
	
		
	public static void main(String[] args){
		Plateau p = new Plateau(8);
		Plateau p2 = new Plateau(8);
		Plateau p3 = new Plateau(8);
		
		p3.setCase(5, 3, 0);
		p3.setCase(4, 3, 0);
		System.out.println(p);
		
		if(p.equals(p2)){
			System.out.println("p = p2");
		}
		else{
			System.out.println("p != p2");
		}
		
		if(p.equals(p3)){
			System.out.println("p = p3");
		}
		else{
			System.out.println("p != p3");
		}
		
		
		
	}


}
