package ollehto;

public class JoueurOllehto extends Joueur{
	private int couleur;
	private Joueur adversaire;
	
	
	public JoueurOllehto(String nomJoueur, int couleur, boolean estOrdi) {
		super(nomJoueur, estOrdi);
		this.couleur = couleur;
	}
	
	public void setAdversaire(Joueur j){
		this.adversaire = j;
	}
	
	public int getCouleur(){
		return this.couleur;
	}	
	
	public Joueur getAdversaire(){
		return this.adversaire;
	}
}
